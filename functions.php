<?php
/**
 * Core functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Core
 */

// ! REMOVE ADMIN BAR
add_filter('show_admin_bar', '__return_false');

if ( ! function_exists( 'core_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function core_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Core, use a find and replace
	 * to change 'core' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'core', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'core' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'core_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 250,
		'width'       => 250,
		'flex-width'  => true,
		'flex-height' => true,
	) );
}
endif;
add_action( 'after_setup_theme', 'core_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function core_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'core_content_width', 640 );
}
add_action( 'after_setup_theme', 'core_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function core_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'core' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'core' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'core_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function core_scripts() {

	//wp_enqueue_style( 'core-style', get_stylesheet_uri(), array(), time() );
	//wp_enqueue_style( 'core-style', get_stylesheet_directory_uri().'/css/style.css', array(), time() );
	wp_enqueue_style( 'core-style', get_stylesheet_directory_uri().'/css/style.css', array(), null );

	wp_enqueue_script( 'core-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'core-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'core_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/* Custom Functions */

function custom_scripts() {

	//wp_enqueue_style('bootstrap_style', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
	//wp_enqueue_style('fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	//wp_enqueue_style('customp_style', get_template_directory_uri().'/custom.css', array(), time());
		
	// remove gutenberg block css
	wp_dequeue_style( 'wp-block-library' );
  wp_deregister_style( 'wp-block-library' );

	wp_enqueue_script('bootstrap_js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
}
add_action('wp_enqueue_scripts', 'custom_scripts');

function jm_load_scripts() {
	global $is_IE;

  if ( !is_admin() ) {
	  wp_deregister_script( 'jquery' );
		if( !LOCALDEV )
			wp_register_script ( 'jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" , false, '3.3.1', false);
		else
			wp_register_script ( 'jquery', get_template_directory_uri() . "/js/jquery.min.js" , false, '3.3.1', false);
		wp_enqueue_script ( 'jquery' );

		//wp_register_script ( 'jquery', get_template_directory_uri() . "/js/scripts.min.js" , 'jquery', '0.0.1', true);
		//wp_enqueue_script ( 'jquery' );

		if ($is_IE) {
			wp_register_script ( 'html5shiv', "https://html5shiv.googlecode.com/svn/trunk/html5.js" , false, true);
			wp_enqueue_script ( 'html5shiv' );
		} 
	}
}
add_action( 'init', 'jm_load_scripts' );

// Remove emojis
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
?>
