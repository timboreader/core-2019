<?php
	/*
 if( !current_user_can('administrator') ) {
	wp_redirect('/holding.html');	
} 	
*/
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Core
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-104890523-1', 'auto');
  ga('send', 'pageview');

	</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-W84TCZB');</script>
	<!-- End Google Tag Manager -->
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W84TCZB" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php wp_head(); ?>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>

	<script type="text/javascript">
		jQuery(document).ready(function() {

		    // Put your offset checking in a function
		    function checkOffset() {
		        if (jQuery(".navbar").offset().top > 50) {
		            jQuery(".navbar-fixed-top").addClass("top-nav-collapse");
		        }     
		        else {
		            jQuery(".navbar-fixed-top").removeClass("top-nav-collapse");
		        }
		    }

		    // Run it when the page loads
		    checkOffset();

		    // Run function when scrolling
		    jQuery(window).scroll(function() {
		        checkOffset();
		    });

		});
	</script>

	<script src="https://use.typekit.net/qdn8hfb.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

</head>

<body <?php body_class(); ?>>

	<?php if( is_front_page() || is_page('gift-experiences') || is_page('the-bar-at-core')) { ?>		

	<header>
		<div id="scroll-down">
			<a href="#intro"><img src="/wp-content/uploads/2017/07/core-chevron-down-1.png" id="arrow-icon"></a>
		</div>
	</header>
	
	<?php } ?>

	<nav class="navbar navbar-inverse navbar-custom navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Navbar">
					<span class="bar-line"></span>
		      <span class="bar-line"></span>
		      <span class="bar-line"></span>              
				</button>
			</div>
			<div class="collapse navbar-collapse" id="Navbar">
				<ul class="nav navbar-nav">

					<?php if( is_page('gift-experiences') || is_page('the-bar-at-core') || is_page_template( 'page-policy.php' )) { ?>		

					<li><a class="" href="<?php echo get_site_url(); ?>"><img src="/wp-content/uploads/2017/07/core-chevron-left-1.png" /> Back to CORE restaurant</a></li>

					<?php } else { ?>
		
					<li><a class="" href="#reservations" data-toggle="collapse" data-target=".navbar-collapse.in">Make a reservation</a></li>
  	      <li><a href="/gift-experiences/">View gift experiences</a></li>
  	      <li><a class="" href="tel:442039375086" data-toggle="collapse" data-target=".navbar-collapse.in">Call us</a></li>
  	      <li><a class="" target="_blank" href="https://www.google.co.uk/maps/place/Notting+Hill+Brasserie,+92+Kensington+Park+Rd,+London+W11+2PN/data=!4m2!3m1!1s0x48760fe257249d67:0xe46f1dfec21018c0?sa=X&ved=0ahUKEwi49M2DwZrVAhWJKlAKHYIjAyUQ8gEIKDAA" data-toggle="collapse" data-target=".navbar-collapse.in">Find us</a></li>
  	      <span class="mobile-nav">
						<li><a class="" href="#about" data-toggle="collapse" data-target=".navbar-collapse.in">About Core</a></li>
		        <li><a class="" href="#clare" data-toggle="collapse" data-target=".navbar-collapse.in">Clare Smyth</a></li>
		        <li><a class="" href="#core-team" data-toggle="collapse" data-target=".navbar-collapse.in">Core Team</a></li>
		        <li><a class="" href="#gallery" data-toggle="collapse" data-target=".navbar-collapse.in">Gallery</a></li>
		        <li><a class="" href="#menus" data-toggle="collapse" data-target=".navbar-collapse.in">Menus</a></li>
		        <li><a class="" href="/the-bar-at-core" data-toggle="collapse" data-target=".navbar-collapse.in">The Bar at Core</a></li>
		        <li><a class="" href="#newsletter" data-toggle="collapse" data-target=".navbar-collapse.in">Sign Up</a></li>
		        <li><a class="" href="#careers" data-toggle="collapse" data-target=".navbar-collapse.in">Careers</a></li>
		        <!-- <li><a class="" href="#slice-reserve" data-toggle="collapse" data-target=".navbar-collapse.in">Make a reservation</a></li> -->
  	      </span>

  	      <?php } ?>

				</ul>
				<div class="navbar-right">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#homeNav" style="display: block !important;">
						<span class="bar-line"></span>
			      <span class="bar-line"></span>
			      <span class="bar-line"></span>              
					</button>
				</div>
			</div>
			<div class="collapse navbar-collapse" id="homeNav">
				<ul class="nav navbar-nav cus-nav">
						<li><a class="" href="/#about" data-toggle="collapse" data-target=".navbar-collapse.in">About Core</a></li>
		        <li><a class="" href="/#clare" data-toggle="collapse" data-target=".navbar-collapse.in">Clare Smyth</a></li>
		        <li><a class="" href="/#core-team" data-toggle="collapse" data-target=".navbar-collapse.in">Core Team</a></li>
		        <li><a class="" href="/#gallery" data-toggle="collapse" data-target=".navbar-collapse.in">Gallery</a></li>
		        <li><a class="" href="/#menus" data-toggle="collapse" data-target=".navbar-collapse.in">Menus</a></li>
		        <li><a class="" href="/the-bar-at-core" data-toggle="collapse" data-target=".navbar-collapse.in">The Bar at Core</a></li>
		        <li><a class="" href="/#newsletter" data-toggle="collapse" data-target=".navbar-collapse.in">Sign Up</a></li>
		        <li><a class="" href="/#careers" data-toggle="collapse" data-target=".navbar-collapse.in">Careers</a></li>
		        <!-- <li><a class="" href="#slice-reserve" data-toggle="collapse" data-target=".navbar-collapse.in">Make a reservation</a></li> -->
				</ul>
			</div>
		</div>
	</nav>

	<div class="gift-experiences-inquire" id="fancybox1" style="display: none; text-align: center;">
		<div class="form-outer-wrap">
		<?php echo do_shortcode('[contact-form-7 id="1130" title="Enquire Form"]'); ?>
		</div>
	</div>

	<div id="content" class="site-content">
