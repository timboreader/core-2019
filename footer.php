<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Core
 */
?>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info container">
			<div class="col-md-9">
				<h4 class="core">CORE</h4>
				<p><?php the_field('address', 4); ?></p>
				<!--<p><a href="<?php echo 'tel:'.preg_replace('/\s/', '', get_field('tel_no')); ?>"><?php echo get_field('tel_no'); ?></a></p>-->
				<p><a class="" href="tel:442039375086">020 3937 5086</a></p>
				<?php the_field('contact_info', 4); ?>
				<h4 class="opening">OPENING TIMES</h4>
				<?php the_field('opening_times', 4); ?>
				<p style="margin-top: 2em;"><a href="/website-privacy-notice/">Privacy Policy</a> | <a href="/core-cookie-policy/">Cookie Policy</a> | <a href="/terms-of-use/">Terms of Use</a></p>

			</div>
			<div class="col-md-2 pull-right">
				<ul class="social-icons-footer">
					<li><a href="https://www.instagram.com/corebyclaresmyth/"><i class="fa fa-instagram" aria-hidden="true"></a></i></li>
					<li><a href="https://www.facebook.com/Core-644726912389183/"><i class="fa fa-facebook" aria-hidden="true"></a></i></li>
				</ul>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

<?php wp_footer(); ?>

<script type="text/javascript">

	jQuery(document).ready(function($){

		var teamdescr = $('#core-team').find('.panel-first-child');
		var teambiogs = $('#core-team').find('.panel-last-child');
		teambiogs.hide();

		// biographies button
		$(document).on('click', '#biog', function(e) {
			e.preventDefault();

			// jm_ page jumps down when the team panel fades out
			// so we only fade out when the bios panel has finished fading in
			teambiogs.fadeIn(
				100,
				function () { 
					teamdescr.fadeOut(100);
					$('html, body').stop().animate(
						{
						'scrollTop': $('#core-team').offset().top
						}, 
						800, 
						'swing', 
						function () {
							window.location.hash = 'core-team';
						}
					);
				}
			);

		});

		// menu tabs
		<?php if (is_page('the-bar-at-core')) { ?>
		var menus = ['drinks', 'snacks'];
		<?php } else { ?>
		var menus = ['tasting-menu', 'small-tasting-menu', 'a-la-carte-menu'];
		<?php } ?>
		$(document).on('click', '.menu-tab', function(e) {
			// stop normal click behaviour
			e.preventDefault();
			
			// find which menu
			var menu_id = $(this).attr('id');
			var menu_tab = $('#'+menu_id);
			var menu_panel = $('#'+menu_id.replace('-tab', ''));

			// hide all menus and make each tab inactive
			menus.forEach(hideMenus);
			menus.forEach(deactivateTabs);
			
			// show the menu and make appropriate tab active
			menu_panel.show();
			menu_tab.addClass('active');
		});
		// here's our function to hide menus
		function hideMenus(menu){
			$('#'+menu).hide();
		}
		// here's our function to deactivate the tabs
		function deactivateTabs(menu){
			$('#'+menu+'-tab').removeClass('active');
		}
	});

</script>

<?php if ( 'https://www.corebyclaresmyth.com' != get_bloginfo( 'url' )) $whichsite = 'STAGING'; else $whichsite = 'LIVE'; ?>

<!-- This is the <?php echo $whichsite; ?> site and it's <?php time('Y'); ?> --> 

</body>
</html>
